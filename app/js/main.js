$('.field-container').on('click', function() {
  $(this).find('.placeholder').addClass('active');
  $(this).find('.text-field').focus();
});

$('.field-container input').on('focus', function() {
  $(this).prev().addClass('active');
});

$('.text-field').on('blur', function() {
  if(!$(this).val()) {
    $(this).prev().removeClass('active');
  }
});


$(document).ready(function() {
  $('select').niceSelect();

  $('.terms-click').click(function(e) {
    // e.preventDefault();
    $('html, body').animate({
      scrollTop: $('#terms').offset().top
    }, 1000);
    $("#terms").toggleClass('open');
  });
});