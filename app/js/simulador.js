//Variables para calcular las cuotas
var sliderMonto = document.getElementById('rango');
var sliderCuotas = document.getElementById('rango2');

var outputMonto = document.getElementById('valor');
var outputCuotas = document.getElementById('valor2');

var valorMonto = sliderMonto.value; //Monto seleccionado
var cantidadCuotas = sliderCuotas.value; //Cuotas seleccionadas

var tasaInteresPrimera = 0.9804;
var tasaInteresSegunda = 0.9301;

//Variables para mostrar resultados
var botonSolicitar = document.getElementById('solicitar');

var resultado = document.getElementById('resultado');

var nombre = document.getElementById('nombre');

var moduloFormulario = document.getElementById('modulo-formulario');
var moduloAhorroSi = document.getElementById('modulo-ahorro-si');
var moduloAhorroNo = document.getElementById('modulo-ahorro-no');
var moduloSimulador = document.getElementsByClassName('simulador');
var moduloSlider = document.getElementsByClassName('interno');
var respuestaSlider = document.getElementsByClassName('respuesta-simulador');
var moduloMain = document.getElementsByClassName('modulo-main');
var legales = document.getElementById('legales');



//Centrado de los valores en Sliders
function getOffset(slider, output, isMonto) {
    var sliderMontoWidth = slider.getBoundingClientRect().width;
    var outputWidth = output.getBoundingClientRect().width;

    var ajusteMaximo = 12;
    var widthMonto = outputWidth/2;
    var sliderProgress = (slider.value - slider.min) / (slider.max - slider.min);
    var ajuste = ajusteMaximo * (0.5 - sliderProgress)/0.5;

    if (sliderProgress > 0.5)  {
        ajuste = -ajusteMaximo * (sliderProgress - 0.5) /0.5;
        if (sliderProgress > 0.9 && isMonto) {
        ajuste = -ajusteMaximo * (1.6 - 0.5) /0.5;    
        }
    } 
    return sliderProgress * sliderMontoWidth + ajuste;
}

//SLIDERS
//Monto
sliderMonto.oninput = function() {
    valorMonto = parseInt(this.value); //String a number
    
    // if(valorMonto > 70000) valorMonto = 70000; //Monto a prestar hasta 70mil
    
    if(valorMonto >= 2000) {
        outputMonto.innerHTML = '<span>' + '$' + (valorMonto/1000).toFixed(3) + '</span>';
    } else {
        outputMonto.innerHTML = '<span>' + '$' + valorMonto+ '</span>';    
    }
    
    outputMonto.setAttribute('style', 'left: ' + getOffset(this, outputMonto, true) + 'px');
        
    simular();    
}


//Cambiar pasos de cuotas (Desde cuota 3 hasta 30, de a 3 - Desde cuota 30 hasta 48, de a 6)
function getCuotas(step) {
    cuotas = step * 3;

    if (step > 10) {
        cuotas = ((step - 10) * 6) + 30;
    }
    
    return cuotas;
}


//Cuotas
sliderCuotas.oninput = function() {
    cantidadCuotas = getCuotas(this.value);
    outputCuotas.innerHTML = '<span>' + cantidadCuotas +'</span>';
    
    var sliderCuotasWidth = this.getBoundingClientRect().width;
    var outputCuotasWidth = outputCuotas.getBoundingClientRect().width;
    var offset = this.value / (this.max - this.min) * sliderCuotasWidth - outputCuotasWidth / 2;
    
    outputCuotas.setAttribute('style', 'left: ' + getOffset(this, outputCuotas) + 'px');
    

    simular();
}

sliderMonto.oninput();
sliderCuotas.oninput();





//CÁLCULO DEL PRÉSTAMO

//Calcula el monto MÁS el costo fijo
function getMonto() {
    if ((valorMonto >= 2000) && (valorMonto <= 4999)) {
        return valorMonto + 300; //De modificarse el costo fijo, cambiar este valor.
        
    } else if ((valorMonto >= 5000) && (valorMonto <= 9999)) {
        return valorMonto + 700;
        
    } else if ((valorMonto >= 10000) && (valorMonto <= 14999)) {
        return valorMonto + 1200;
      
    } else if ((valorMonto >= 15000) && (valorMonto <= 19999)) {
        return valorMonto + 1800;
        
    } else if ((valorMonto >= 20000) && (valorMonto <= 29999)) {
        return valorMonto + 2500;
        
    } else if ((valorMonto >= 30000) && (valorMonto <= 39999)) {
        return valorMonto + 3500;
      
    } else if ((valorMonto >= 40000)) {
        return valorMonto + 4500;        
    }
}

//Calcula la tasa según la cantidad de cuotas
function getTasa(){
    var tasa = tasaInteresPrimera;
    if (cantidadCuotas > 21) tasa = tasaInteresSegunda;
    tasa = tasa/(365/30)*1.21;
    return tasa;
}


function simular() {
    var montoMasFijo = parseInt(getMonto()); //Monto a prestar MÁS cargo fijo
    var tasaInteres = getTasa(); //TNA de acuerdo a las cuotas
    var pesos = '$';
    console.log('Monto + costo fijo = ' + montoMasFijo);
    console.log('Tasa interés correspondiente = ' + tasaInteres);
    
    var valorCuota = - PMT(tasaInteres, cantidadCuotas, montoMasFijo);
    console.log('Valor de la cuota = ' + valorCuota);
    
    //Formateo del número al exceder mil
    if(valorCuota < 1000) {
        $(resultado).text('$' + ((valorCuota).toFixed(0)));
    } else {
        $(resultado).text('$' + ((valorCuota/1000).toFixed(3)));
    }
}

//Cálculo del préstamo
function PMT(ir, np, pv, fv, type) {
    /*
     * ir   - interés mensual
     * np   - meses
     * pv   - valor actual
     * fv   - valor futuro
     * type - when the payments are due:
     *        0: end of the period, e.g. end of month (default)
     *        1: beginning of period
     */
    var pmt, pvif;

    fv || (fv = 0);
    type || (type = 0);

    if (ir === 0)
        return -(pv + fv)/np;

    pvif = Math.pow(1 + ir, np);
    pmt = - ir * pv * (pvif + fv) / (pvif - 1);

    if (type === 1)
        pmt /= (1 + ir);

    return pmt;
}


//Respuestas a la solicitud
//Posee caja de ahorro o cuenta corriente
function ahorroSi() {
    $(moduloFormulario).hide();
    $(moduloSlider).hide();
    $(moduloAhorroSi).fadeIn(1000);
    $(respuestaSlider).fadeIn(1000);
    $(moduloMain).css('margin-top', '67px');

    var windowWidth = $(window).width();
    if(windowWidth > 1024){
        $(legales).css('margin-top', '61px');    
    }
}

//No posee caja de ahorro o cuenta corriente
function ahorroNo() {
    $(moduloFormulario).hide();
    $(moduloSlider).hide();
    $(moduloAhorroNo).fadeIn(1000);
    $(respuestaSlider).fadeIn(1000);    
    $(moduloMain).css('margin-top', '69px');
    var windowWidth = $(window).width();
    if(windowWidth > 1024){
        $(legales).css('margin-top', '61px');
    }
}

//Almacena las variables para mostrarlas
function almacenarVariables() {
    $('.nombre-mostrar').text(', ' + nombre.value); 
    $('#cifra-prestamo').text('$' + (valorMonto/1000).toFixed(3));
    $('#cant-cuotas').text(cantidadCuotas);
}

//Click en SOLICITAR
function solicitar() {
    
        almacenarVariables();
            
        var cajaAhorroSi = $('#ahorro-si').is(':checked');
            
        if (cajaAhorroSi) {
            ahorroSi();           
        } else {
            ahorroNo();   
        }
}
