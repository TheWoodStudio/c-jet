#Gulp Setup
Install Node.
If you do not have Node installed already, you can get it from https://nodejs.org/

#Install Gulp (Use 'sudo' if running on mac)
$ npm install gulp -g

#from within our project folder
$ npm install

#app folder
used to edit code

#dist folder
used for production

#Watch to work on the project
$ gulp watch

#Build app folder for deployment
$ gulp build